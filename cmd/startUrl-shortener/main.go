package main

import (
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/go_server/internal/config"
	"gitlab.com/go_server/internal/http-server/handlers/url/save"
	"gitlab.com/go_server/internal/http-server/middleweare/logger"
	"gitlab.com/go_server/internal/lib/logger/sl"
	"gitlab.com/go_server/internal/storage/sqlite"
	"golang.org/x/exp/slog"
	"net/http"
	"os"
)

const (
	envLocal = "local"
	envDev   = "dev"
	envProd  = "prod"
)

func main() {
	//init config: cleanenv
	cfg := config.MustLoad()

	fmt.Println(cfg) //удалить перед продакшенм обязательно

	//init logger: slog
	logSlog := setupLogger(cfg.Env)

	logSlog.Info("starting url-shorter", slog.String("env", cfg.Env)) // выведем в первом логе в каком окружении мы работаем(локал, продакт, дев и др)
	logSlog.Debug("debug messages are enabled")

	//init storage: sqlite

	storage, err := sqlite.NewStorage(cfg.StoragePath)
	if err != nil {
		logSlog.Error("failed to init storage", sl.Err(err)) //создадим в lib/logger/sl  ф-ию для добавления параметров в наши ошибки
		os.Exit(1)
	}
	_ = storage

	//init router: chi, "chi render"
	router := chi.NewRouter()
	//используем middlewear
	router.Use(middleware.RequestID) //добавляет к каждому запросу requestID
	router.Use(middleware.RealIP)    //показывает IP пользователя который к вам постучался

	//router.Use(middleware.Logger) // логирует красиво запросы/
	// НАПИШЕМ ЕГО САМИ в http-server/middleweare/logger чтоб эти логи собирались нашим логером slog
	router.Use(logger.New(logSlog))

	router.Use(middleware.Recoverer) // востанавливает панику
	router.Use(middleware.URLFormat) // чтоб писать красивые урлы при подключениии к роутеру или получать параметры в хендлере

	router.Post("/url", save.New(logSlog, storage))

	logSlog.Info("starting server", slog.String("address", cfg.Address))

	// run server
	srv := &http.Server{
		Addr:         cfg.Address,
		Handler:      router,
		ReadTimeout:  cfg.HTTPServer.Timeout,
		WriteTimeout: cfg.HTTPServer.Timeout,
		IdleTimeout:  cfg.HTTPServer.IdleTimeout,
	}

	if err := srv.ListenAndServe(); err != nil {
		logSlog.Error("failed to start server")
	}

	logSlog.Error("server stopped")
}

// выносим отдельно ф-ию с входным параметром env чтоб в разных ситуациях показывать разные уровни логирования
func setupLogger(env string) *slog.Logger {
	var log *slog.Logger
	switch env {
	case envLocal:
		log = slog.New(
			slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}),
		)
	case envDev:
		log = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}),
		)
	case envProd:
		log = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelInfo}),
		)

	}
	return log
}
