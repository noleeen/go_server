package sqlite

import (
	"database/sql"
	"errors"
	"fmt"
	"github.com/mattn/go-sqlite3"
	"gitlab.com/go_server/internal/storage"

	_ "github.com/mattn/go-sqlite3" //init sqlite3 driver если мы не сипользуем этот пакет в коде (в данном случае мы используем поэтому тут дублирование)
)

type Storage struct {
	db *sql.DB
}

func NewStorage(storagePath string) (*Storage, error) {
	const op = "storage.sqlite.NewStorage"

	db, err := sql.Open("sqlite3", storagePath)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	//если таблица url не сущуствует то мы её создаём, если она существует - ничего не произойдёт
	stmt, err := db.Prepare(`
	CREATE TABLE IF NOT EXISTS url( 
		id INTEGER PRIMARY KEY,
		alias TEXT NOT NULL UNIQUE,
		url TEXT NOT NULL);
	CREATE INDEX IF NOT EXISTS idx_alias ON url(alias);
	`)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	_, err = stmt.Exec()
	if err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	return &Storage{db: db}, nil
}

func (s *Storage) SaveURL(urlToSave string, alias string) (int64, error) {
	const op = "storage.sqlite.SaveURL"

	stmt, err := s.db.Prepare("INSERT INTO url(url, alias) VALUES (?, ?)") //ПОДГОТАВЛИВАЕМ зАПРОС
	if err != nil {
		return 0, fmt.Errorf("%s: %w", op, err)
	}

	res, err := stmt.Exec(urlToSave, alias) //запускаем запрос
	if err != nil {
		//если мы добавляем в таблицу урл с алиасом который уже был создан, то вернётся ошибка.
		//сделаем так чтобы эта ошибка была общая для всех стораджей
		if sqliteErr, ok := err.(sqlite3.Error); ok && sqliteErr.ExtendedCode == sqlite3.ErrConstraintUnique {
			return 0, fmt.Errorf("%s: %w", op, err)
		}
	}

	//получаем id
	id, err := res.LastInsertId() //эта ф-ия не работает в mysql (в postgres работает)
	if err != nil {
		return 0, fmt.Errorf("%s: failed to get last insert id %w", op, err)
	}
	return id, nil
}

func (s *Storage) GetURL(alias string) (string, error) {
	const op = "storage.sqlite.GetURL"

	stmt, err := s.db.Prepare("SELECT url FROM url WHERE alias = ?") //ПОДГОТАВЛИВАЕМ зАПРОС
	if err != nil {
		return "", fmt.Errorf("%s: prepare statement: %w", op, err)
	}

	var resURL string

	err = stmt.QueryRow(alias).Scan(&resURL)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return "", storage.ErrURLNotFound
		}
		return "", fmt.Errorf("%s: execute statement: %w", op, err)

	}
	return resURL, nil
}
