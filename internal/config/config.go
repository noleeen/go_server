package config

import (
	"github.com/ilyakaznacheev/cleanenv"
	"log"
	"os"
	"time"
)

type Config struct {
	Env         string `yaml:"env" env:"ENV" env-default:"local"` //env-default - значение по умолчанию, если не объявлено
	StoragePath string `yaml:"storage_path" env-required:"true"`
	HTTPServer  `yaml:"http_server"`
}

type HTTPServer struct {
	Address     string        `yaml:"address" env-default:"localhost:8080"`
	Timeout     time.Duration `yaml:"timeout" env-default:"4s"`
	IdleTimeout time.Duration `yaml:"idle_timeout" env-default:"60s"`
}

func MustLoad() *Config { // приставка must используется если функция вместо возврата ошибки будет паниковать
	configPath := os.Getenv("CONFIG_PATH") //получаем файл с конфигом из переменной окружения
	if configPath == "" {
		configPath = "./config/local.yaml" //это костыль. нужно выяснить как получать конфиг из переменной окружения
		if configPath == "" {
			log.Fatal("CONFIG_PATH is not exist")
		}
	}
	// проверяем существует ли файл
	if _, err := os.Stat(configPath); os.IsNotExist(err) {
		log.Fatal("config file does not exist: %s", configPath)
	}

	var cfg Config
	if err := cleanenv.ReadConfig(configPath, &cfg); err != nil {
		log.Fatal("cannot read config: %s", err)
	}

	return &cfg
}
